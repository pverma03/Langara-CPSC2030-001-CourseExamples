
function outer(){
    let temp = 0;
    function inner(){
        temp = temp+1;
        return temp;
    }

    return inner;
}

let inner1 = outer();
let inner2 = outer();

let element = document.querySelector("h2");
element.onclick = function(event){

    if (event.target.classList.length ==0){
        event.target.classList.add("selected");
    } else {
        event.target.classList.remove("selected");
    }
}