let currentUser;
let currentIdx;
function createDiv(row){
    let msg = "";
    
    msg = "<b>From:</b> "+row.from + "<br>" +
          "<b>Date</b>F" +row.date + "<br>" +
          "<b>Subject:</b>"+row.subject;

    msg = "<div class=\"indexItem\" idx=\""+row.Idx+"\">"+msg+"</div>";    
    return msg;
}
let messageIndex =[];
$(".item").click(function(event){
    
    //choose selected item
    $(".item").removeClass("selected");
    $(event.target).addClass("selected");

    let user = $(event.target).attr("user");
    //generate ajax request
    currentUser = user;
    $.ajax({
        url: "getmaildirectory.php",
        method: "POST",
        data: {
            user: user
        },
        success: function(msg){
           messageIndex = JSON.parse(msg);
           let output = "";  
           for(let i = 0; i < messageIndex.length; i++){
              output += createDiv(messageIndex[i]);
           }
          $(".index").html(output);  
        }
    })
});

$(".index").click(function(event){

    if($(event.target).hasClass("indexItem")){
        //only responds to click of items, not other places
        $(".indexItem").removeClass("selected");
        $(event.target).addClass("selected");
        let ai = $(".indexItem").index(event.target);
        currentIdx = messageIndex[ai].idx;
        $.ajax({
            url:"getmail.php",
            method: "POST",
            data:{
                idx: currentIdx
            },
            success: function(msg){
               let data = JSON.parse(msg);
               $("span.to").html(data.to);
               $("span.from").html(data.from);
               $("span.date").html(data.date);
               $("span.subject").html(data.subject);
               $("span.message").html(data.message);
                
            }
        })
    }
});