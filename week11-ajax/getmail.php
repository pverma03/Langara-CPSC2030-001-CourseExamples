<?php

include "staticdata.php";

if(array_key_exists("idx", $_POST) ){

    foreach ($messageTable as $message){

      if(
         intVal($_POST["idx"]) == $message["Idx"]){
         $tempArr = array("to"=> $message["To"],
                          "from"=> $message["From"],
                          "date"=> $message["Date"],
                          "subject"=> $message["Subject"],
                          "message"=> $message["Message"]);
         echo json_encode($tempArr);
         die();
      }
    }
} 


echo json_encode(array(
         "idx" => 0,
        "to" => "",
        "from" => "",
        "folder" => "",
        "date" => "",
        "subject" => "",
        "message" => "No Message Found"
));

?>