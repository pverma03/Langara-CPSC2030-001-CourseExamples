/* Example of a join */
/* That is join two tables, where 
   -there is a value in both table for the join condition */
/* In this example, create a table for the stores 
   -with items, price and item description. 
   -Most data is in the storeInventory table, 
   -but the description is in the warehouse table */

select store, storeinventory.item, storeinventory.price, warehouse.description
from storeinventory
inner join warehouse on storeinventory.item = warehouse.item;

/* for item, even thought they are the same in both tables
   -we still need to specify which table to get it from 
   -Since in other joins, they may not be the same 
   -for price we have two different prices*/